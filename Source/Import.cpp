#include "Node/Node.h"
#include "Tree.h"

BOOL 
ImportFile(
	const char *filename
){
	CHAR NameBuffer[256];
	CHAR FqdnBuffer[256];
	CHAR PathBuffer[256];
	CHAR VersBuffer[16];
	INT Os; 
	INT Type;
	CHAR IpBuffer[256];
	CHAR SubnetBuffer[256];
	CHAR MacBuffer[256];
	BYTE IsStaticLease;	

	FILE *file;
	file = fopen(filename, "r");
	while (EOF != fscanf(
			file, 
			NODE_CSV_FORMAT,
    			NameBuffer,
    			FqdnBuffer,
			PathBuffer,
			VersBuffer,
			&Os,
    			&Type,
    			IpBuffer,
    			SubnetBuffer,
    			MacBuffer,
    			&IsStaticLease
		)){
			NODE *node = new NODE(
				NameBuffer, 
		    		FqdnBuffer,
				PathBuffer,
				VersBuffer,
				(NODE_OS)Os,
		    		(NODE_TYPE)Type, 
		    		Utils::CreateIpv4AddressFromString(IpBuffer), 
		    		Utils::CreateIpv4AddressFromString(SubnetBuffer), 
		    		Utils::CreateMacAddressFromString(MacBuffer), 
		    		(BOOL)IsStaticLease
			);

			Tree::InsertIntoTree(node);
		}
		
	return true;
}