#include "Utils.h"



IPV4_ADDRESS
Utils::CreateIpv4AddressFromString(
	LPCSTR          lpcszIpv4Address
){
	IPV4_ADDRESS ip4Address;
	sscanf(lpcszIpv4Address, 
		"%hhu.%hhu.%hhu.%hhu",          /* %hhu is format specifier for BYTE (unsigned char) */
		&(ip4Address.byData[0]), 
		&(ip4Address.byData[1]),
		&(ip4Address.byData[2]),
		&(ip4Address.byData[3])
	);
	return ip4Address;
}




BOOL 
Utils::Ipv4AddressToString(
	LPIPV4_ADDRESS  lpip4Address,
	LPSTR           lpszBufferOut,
	ULONG           ulBufferSize
){
	memset(lpszBufferOut, 0x00, ulBufferSize);
	snprintf(lpszBufferOut,
		ulBufferSize,
		"%hhu.%hhu.%hhu.%hhu",
		lpip4Address->byData[0],
		lpip4Address->byData[1],
		lpip4Address->byData[2],
		lpip4Address->byData[3]
	);
	return TRUE;
}




MAC_ADDRESS
Utils::CreateMacAddressFromString(
	LPCSTR          lpcszMacAddress
){
	MAC_ADDRESS maMac;
	sscanf(lpcszMacAddress,
		"%hhX:%hhX:%hhX:%hhX:%hhX:%hhX",
		&(maMac.byData[0]),
		&(maMac.byData[1]),
		&(maMac.byData[2]),
		&(maMac.byData[3]),
		&(maMac.byData[4]),
		&(maMac.byData[5])
	);
	return maMac;
}




BOOL
Utils::MacAddressToString(
	LPMAC_ADDRESS   lpmaMacAddress,
	LPSTR          	lpszBufferOut,
	ULONG           ulBufferSize
){
	memset(lpszBufferOut, 0x00, ulBufferSize);
	snprintf(lpszBufferOut,
		ulBufferSize,
		"%02hhX:%02hhX:%02hhX:%02hhX:%02hhX:%02hhX",
		lpmaMacAddress->byData[0],
		lpmaMacAddress->byData[1],
		lpmaMacAddress->byData[2],
		lpmaMacAddress->byData[3],
		lpmaMacAddress->byData[4],
		lpmaMacAddress->byData[5]
	);
	return TRUE;
}