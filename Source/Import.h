#ifndef IMPORT_H
#define IMPORT_H

#include "Node/Node.h"
#include "Tree.h"

BOOL 
ImportFile(
	const char *filename
);


#endif