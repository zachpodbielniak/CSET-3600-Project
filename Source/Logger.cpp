#include "Logger.h"


GLOBAL_VARIABLE FILE *fLog = NULL;
GLOBAL_VARIABLE LPMUTEX lpmtxLock;


BOOL
Log::WriteLog(
	LPCSTR 		lpcszText
){
	if (NULL == lpcszText) 
	{ return false; }
	
	BOOL bRet;
	bRet = true;

	/* Allocate lpmtxLock if need be */
	if (NULL == lpmtxLock)
	{
		lpmtxLock = (LPMUTEX)malloc(sizeof(MUTEX));
		if (NULL == lpmtxLock)
		{ exit(1); } 
		pthread_mutex_init(lpmtxLock, NULL); 
	}

	/* Lock this resource */
	pthread_mutex_lock(lpmtxLock);

 	/* If the FILE for the LOG is NULL, instantiate it. */
	if (NULL == fLog)
	{
		fLog = fopen("Log.txt", "w");
		if (NULL == fLog)
		{ return false; } 

		bRet &= (0 < fputs("Starting up application...\n", fLog)) ? true : false;
		bRet &= (0 < fputs("Instantiated the Log...\n", fLog)) ? true : false;
	
		#ifdef __DEBUG__ 
		bRet &= (0 < fputs("Starting up application...\n", stdout)) ? true : false;
		bRet &= (0 < fputs("Instantiated the Log...\n", stdout)) ? true : false;
		#endif
	}

	/* If we are in debug mode, also log out to STDOUT */
	#ifdef __DEBUG__
	bRet &= (0 < fputs(lpcszText, stdout)) ? true : false;
	#endif

	bRet &= (0 < fputs(lpcszText, fLog)) ? true : false; 
	pthread_mutex_unlock(lpmtxLock);
	return bRet;
}




BOOL
Log::FlushLog(
	VOID
){
	if (NULL == fLog)
	{ return false; }
	
	fflush(fLog);
	
	#ifdef __DEBUG__
	fflush(stdout);
	#endif

	return true;
}
