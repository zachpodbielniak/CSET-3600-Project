#include "Prereqs.h"
#include "Logger.h"
#include "Gui.h"
#include "Utils.h"
#include "Tree.h"





INT
main(
	INT 		iArgCount,
	DLPSTR 		dlpszArgValues
){
	Log::WriteLog("Started Application\n");


	/* Initialize the GTK Library */
	gtk_init(&iArgCount, &dlpszArgValues);

	/* Create the main window */
	Gui::CreateMainWindow();
	Log::WriteLog("Main Window Was Created.\n");

	Tree::InitializeTree();
	Log::WriteLog("Initialized Tree.\n");

	/* This is required for GTK */
	gtk_main();

  	return 0;

}
