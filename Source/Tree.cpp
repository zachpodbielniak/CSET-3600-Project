#include "Tree.h"



GLOBAL_VARIABLE GObject *goTree;
GLOBAL_VARIABLE GtkTreeStore *gtsTheTree;
GLOBAL_VARIABLE MUTEX mtxLock;
GLOBAL_VARIABLE GtkTreeIter gtiMain;
/* GLOBAL_VARIABLE GtkTreeIter gtiChild; */


GtkTreeView *
Tree::GetTreeViewObject(
	VOID
){
	GObject *goTree;

	goTree = gtk_builder_get_object(Gui::GetMainWindowBuilder(), "gtTree");

	return GTK_TREE_VIEW(goTree);
}


/*
INTERNAL_OPERATION
VOID
__ToggleObject(
	GtkCellRendererToggle 	*gcrtRenderToggle,
	LPGSTR 		lpgszPathString,
	LPGVOID 	lpData
){
	UNREFERENCED_PARAMETER(gcrtRenderToggle);
	UNREFERENCED_PARAMETER(lpgszPathString);
	UNREFERENCED_PARAMETER(lpData);
}
*/


INTERNAL_OPERATION
VOID
__OnRowDoubleClick(
	GtkTreeView 	 	*gtvTree,
	GtkTreePath 		*gtpPath,
	GtkTreeViewColumn 	*gtvcCol,
	LPGVOID 		lpgData
){
	UNREFERENCED_PARAMETER(gtvcCol);
	UNREFERENCED_PARAMETER(lpgData);

	GtkTreeModel 	*gtmModel;
	GtkTreeIter 	gtiIter;
	NODE 		*lpnNode;

	gtmModel = gtk_tree_view_get_model(gtvTree);

	if (!gtk_tree_model_get_iter(gtmModel, &gtiIter, gtpPath))
		return;

	gtk_tree_model_get(
		gtmModel,
		&gtiIter,
		NODE_CLASS, &lpnNode,
		-1
	);

	/* Free the memory allocated */
	delete lpnNode;

	gtk_tree_store_remove(gtsTheTree, &gtiIter);
}




INTERNAL_OPERATION
BOOL
__CreateColumns(
	GtkTreeView *gtvTheTree
){
	GtkCellRenderer *gcrRender;
	GtkTreeViewColumn *gtvcCol;

	gcrRender = gtk_cell_renderer_text_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"Name",
		gcrRender,
		"text",
		BRANCH_NAME,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);

	/* Router */
	gcrRender = gtk_cell_renderer_toggle_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"Router",
		gcrRender,
		"active",
		ROUTER_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);

	/* Switch */
	gcrRender = gtk_cell_renderer_toggle_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"Switch",
		gcrRender,
		"active",
		SWITCH_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);
	
	/* Server Column */
	gcrRender = gtk_cell_renderer_toggle_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"Server",
		gcrRender,
		"active",
		SERVER_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);

	/* OS Column */
	gcrRender = gtk_cell_renderer_text_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"OS",
		gcrRender,
		"text",
		OS_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);

	/* FQDN */
	gcrRender = gtk_cell_renderer_text_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"FQDN",
		gcrRender,
		"text",
		FQDN_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);
	
	/* Path Column*/
	gcrRender = gtk_cell_renderer_text_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"Path",
		gcrRender,
		"text",
		PATH_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);

	/* Vers Column */
	gcrRender = gtk_cell_renderer_text_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"Version",
		gcrRender,
		"text",
		VERS_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);

	/* IP Address */
	gcrRender = gtk_cell_renderer_text_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"IP Address",
		gcrRender,
		"text",
		IP_ADDRESS_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);

	/* Is Static? */
	gcrRender = gtk_cell_renderer_toggle_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"Static IP",
		gcrRender,
		"active",
		IS_STATIC_LEASE_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);

	/* Subnet Address */
	gcrRender = gtk_cell_renderer_text_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"Subnet",
		gcrRender,
		"text",
		SUBNET_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);

	/* MAC Address */
	gcrRender = gtk_cell_renderer_text_new();
	gtvcCol = gtk_tree_view_column_new_with_attributes(
		"MAC Address",
		gcrRender,
		"text",
		MAC_ADDRESS_COLUMN,
		NULL
	);
	gtk_tree_view_append_column(GTK_TREE_VIEW(gtvTheTree), gtvcCol);


	return true;
}



BOOL
Tree::InitializeTree(
	VOID
){
	GtkBuilder *gbMainWindow;
	pthread_mutex_init(&mtxLock, NULL);
	
	gbMainWindow = Gui::GetMainWindowBuilder();
	if (NULL == gbMainWindow)
	{ return false; }

	/* Lock this resource */
	pthread_mutex_lock(&mtxLock);


	/* Create the tree */
	gtsTheTree = gtk_tree_store_new(
		TREE_NUM_COLUMNS,
		G_TYPE_STRING,		/* Name */
		G_TYPE_BOOLEAN,		/* Router */
		G_TYPE_BOOLEAN,		/* Switch */
		G_TYPE_BOOLEAN,		/* Server */
		G_TYPE_STRING, 		/* OS */
		G_TYPE_STRING,		/* FQDN */
		G_TYPE_STRING, 		/* Path */
		G_TYPE_STRING, 		/* Version */
		G_TYPE_STRING,		/* IP Address */
		G_TYPE_BOOLEAN,		/* Static Lease? */
		G_TYPE_STRING, 		/* Subnet */
		G_TYPE_STRING,		/* MAC Address */
		G_TYPE_POINTER 		/* NODE Data */
	);

	
	goTree = gtk_builder_get_object(gbMainWindow, "gtTree");
	/* Create the tree view from the model */
	/*gwTree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(gtsTheTree));*/
	gtk_tree_view_set_model(GTK_TREE_VIEW(goTree), GTK_TREE_MODEL(gtsTheTree));

	/*g_object_unref(gtsTheTree);*/
	gtk_tree_selection_set_mode(
		gtk_tree_view_get_selection(GTK_TREE_VIEW(goTree)),
		GTK_SELECTION_SINGLE
	);


	/* Create the columns */
	__CreateColumns(GTK_TREE_VIEW(goTree));


	/* Some magic to make all rows expanded 
	g_signal_connect(
		gwTree,
		"realize",
		G_CALLBACK(gtk_tree_view_expand_all),
		NULL
	);*/

	
	g_signal_connect(
		goTree,
		"row-activated",
		G_CALLBACK(__OnRowDoubleClick),
		NULL
	);
	



	/* Unlock resource */
	pthread_mutex_unlock(&mtxLock);

	return true;
}



BOOL
Tree::InsertIntoTree(
	NODE 		*lpnNode
){
	if (NULL == lpnNode)
	{ return false; }

	pthread_mutex_lock(&mtxLock);

	CHAR szIpBuffer[0x20];
	CHAR szSubnetBuffer[0x20];
	CHAR szMacBuffer[0x20];

	memset(szIpBuffer, 0x00, sizeof(szIpBuffer));
	memset(szSubnetBuffer, 0x00, sizeof(szSubnetBuffer));
	memset(szMacBuffer, 0x00, sizeof(szMacBuffer));

	Utils::Ipv4AddressToString(
		&(lpnNode->GetNodeCore()->ip4Address),
		szIpBuffer,
		sizeof(szIpBuffer)
	);

	Utils::Ipv4AddressToString(
		&(lpnNode->GetNodeCore()->ip4Subnet),
		szSubnetBuffer,
		sizeof(szSubnetBuffer)
	);

	Utils::MacAddressToString(
		&(lpnNode->GetNodeCore()->maMacAddress),
		szMacBuffer,
		sizeof(szMacBuffer)
	);


	gtk_tree_store_append(gtsTheTree, &gtiMain, NULL);

	gtk_tree_store_set(
		gtsTheTree,
		&gtiMain,
		BRANCH_NAME, lpnNode->GetNodeCore()->szName,
		ROUTER_COLUMN, (NODE_TYPE_ROUTER == lpnNode->GetNodeCore()->ntType) ? TRUE : FALSE,
		SWITCH_COLUMN, (NODE_TYPE_SWITCH == lpnNode->GetNodeCore()->ntType) ? TRUE : FALSE,
		SERVER_COLUMN, (NODE_TYPE_HOST == lpnNode->GetNodeCore()->ntType) ? TRUE : FALSE,
		FQDN_COLUMN, lpnNode->GetNodeCore()->szFqdn,
		PATH_COLUMN, lpnNode->GetNodeCore()->szPath,
		VERS_COLUMN, lpnNode->GetNodeCore()->szVers,
		OS_COLUMN, NODE_OS_TO_STR(lpnNode->GetNodeCore()->nosOs),
		IP_ADDRESS_COLUMN, szIpBuffer,
		IS_STATIC_LEASE_COLUMN, !(lpnNode->GetNodeCore()->bIsDynamicLease),
		SUBNET_COLUMN, szSubnetBuffer,
		MAC_ADDRESS_COLUMN, szMacBuffer,
		NODE_CLASS, lpnNode,
		-1
	);

	lpnNode->SetIter(gtiMain);	

	pthread_mutex_unlock(&mtxLock);

	return true;
}




NODE *
Tree::TreeWalk(
	GtkTreeIter *gtiIter,
	BOOL bFirst
){
	GtkTreeModel *gtmModel;
	NODE *lpnNodeOut;
	if (NULL == gtiIter)
	{
		Log::WriteLog("gtkIter was NULL on call to TreeWalk()\n");
		return NULL;
	}

	gtmModel = gtk_tree_view_get_model(GTK_TREE_VIEW(goTree));

	if (NULL == gtmModel)
	{
		Log::WriteLog("GtkTreeModel * was NULL!\n");
		return NULL;
	}

	if (true == bFirst)
	{ gtk_tree_model_get_iter_first(gtmModel, gtiIter); }
	else
	{
		if (!gtk_tree_model_iter_next(gtmModel, gtiIter))
		{ return NULL; }
	}
	

	gtk_tree_model_get(
		gtmModel,
		gtiIter,
		NODE_CLASS, &lpnNodeOut,
		-1
	);


	return lpnNodeOut;
}