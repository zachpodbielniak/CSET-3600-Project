#include "Prereqs.h"
#include "Logger.h"
#include "Utils.h"
#include "Node/Node.h"
#define BOOST_TEST_MODULE Main
#include <boost/test/included/unit_test.hpp>


BOOST_AUTO_TEST_SUITE(Test_Suite)



BOOST_AUTO_TEST_CASE(Ipv4_Test)
{
	IPV4_ADDRESS ipv4Test = Utils::CreateIpv4AddressFromString("200.175.10.48");
	ULONG ulData = 0x300AAFC8; /* Above IP address stored in little endian format */
	CHAR szBuffer[64];
	BOOST_CHECK_EQUAL(Utils::Ipv4AddressToString(&ipv4Test, szBuffer, sizeof(szBuffer)), TRUE);
	BOOST_CHECK_EQUAL(ipv4Test.ulData, ulData);
	BOOST_CHECK_EQUAL(szBuffer, "200.175.10.48");
}



BOOST_AUTO_TEST_CASE(Mac_Test)
{
	MAC_ADDRESS maMacAddress = Utils::CreateMacAddressFromString("DE:AD:00:00:BE:EF");
	ULONGLONG ullData = 0xEFBE0000ADDE;	/* Above MAC Address stored in little endian format */
	CHAR szBuffer[64];
	BOOST_CHECK_EQUAL(Utils::MacAddressToString(&maMacAddress, szBuffer, sizeof(szBuffer)), TRUE);
	BOOST_CHECK_EQUAL(maMacAddress.ullData, ullData);
	BOOST_CHECK_EQUAL(szBuffer, "DE:AD:00:00:BE:EF");
}




BOOST_AUTO_TEST_CASE(Node_Test)
{
	NODE *nTest = new NODE(
		"test",
		"test.example.com",
		NODE_TYPE_SWITCH,
		Utils::CreateIpv4AddressFromString("10.0.0.1"),
		Utils::CreateIpv4AddressFromString("255.0.0.0"),
		Utils::CreateMacAddressFromString("DE:AD:00:00:BE:EF"),
		FALSE
	);

	IPV4_ADDRESS ip4Address = Utils::CreateIpv4AddressFromString("10.0.0.1");
	IPV4_ADDRESS ip4Subnet = Utils::CreateIpv4AddressFromString("255.0.0.0");
	MAC_ADDRESS maMacAddress = Utils::CreateMacAddressFromString("DE:AD:00:00:BE:EF");

	BOOST_CHECK_EQUAL(nTest->GetNodeCore()->lpszName, (LPSTR)"test");
	BOOST_CHECK_EQUAL(nTest->GetNodeCore()->lpszFqdn, (LPSTR)"test.example.com");
	BOOST_CHECK_EQUAL(nTest->GetNodeCore()->ntType, NODE_TYPE_SWITCH);

	BOOST_CHECK_EQUAL(*(ULONG *)&(nTest->GetNodeCore()->ip4Address), *(ULONG *)&(ip4Address));
	BOOST_CHECK_EQUAL(*(ULONG *)&(nTest->GetNodeCore()->ip4Subnet), *(ULONG *)&(ip4Subnet));
	BOOST_CHECK_EQUAL(*(ULONGLONG *)&(nTest->GetNodeCore()->maMacAddress), *(ULONGLONG *)&(maMacAddress));

	BOOST_CHECK_EQUAL(nTest->GetNodeCore()->bIsDynamicLease, FALSE);
	delete nTest;
}


BOOST_AUTO_TEST_CASE(Node_Dump_Test)
{
	NODE *nTest = new NODE(
		"test",
		"test.example.com",
		NODE_TYPE_SWITCH,
		Utils::CreateIpv4AddressFromString("10.0.0.1"),
		Utils::CreateIpv4AddressFromString("255.0.0.0"),
		Utils::CreateMacAddressFromString("DE:AD:00:00:BE:EF"),
		FALSE
	);

	BOOST_CHECK_EQUAL(
		nTest->DumpToString(), 
		"test\ntest.example.com\n1\n10.0.0.1\n255.0.0.0\nDE:AD:00:00:BE:EF\n0"
	);

	delete nTest;
}



BOOST_AUTO_TEST_SUITE_END()
