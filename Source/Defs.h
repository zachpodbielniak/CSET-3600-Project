#ifndef DEFS_H
#define DEFS_H



#define MAIN_WINDOW_UI          "../UI/Main.ui"
#define MAIN_WINDOW_TITLE	"Group 3 Project"
#define MAIN_WINDOW_WIDTH	1000
#define MAIN_WINDOW_HEIGHT 	750

#define ABOUT_WINDOW_UI         "../UI/About.ui"
#define ABOUT_WINDOW_TITLE      "About Our Project"
#define ABOUT_WINDOW_WIDTH      400
#define ABOUT_WINDOW_HEIGHT     300

#define HELP_WINDOW_UI          "../UI/Help.ui"
#define HELP_WINDOW_TITLE       "Get Help"
#define HELP_WINDOW_WIDTH       400
#define HELP_WINDOW_HEIGHT      300

#define PROP_WINDOW_UI          "../UI/Properties2.ui"
#define PROP_WINDOW_TITLE       "Node Properties"
#define PROP_WINDOW_WIDTH       400
#define PROP_WINDOW_HEIGHT      600



#endif
