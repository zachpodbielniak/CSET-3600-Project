#include "Export.h" 

BOOL
ExportFile(
	const char *szFileName,
	BOOL bCsv	
){
	FILE *fTheFile;
	NODE *lpnNode;
	CHAR szBuffer[0x100];
	GtkTreeIter gtiIter;
	LPSTR lpszAllocOut;

	fTheFile = fopen(szFileName,"w");

	if (NULL == fTheFile)
	{
		Log::WriteLog("Failed to open the file for output!\n");
		return false;
	}
	
	snprintf(
		szBuffer,
		0x100,
		"Opened file %s for output.\n",
		szFileName
	);
	Log::WriteLog(szBuffer); 

	lpnNode = Tree::TreeWalk(&gtiIter, true);

	do 
	{
		if (NULL == lpnNode)
		{ break; }

		switch (bCsv)
		{

			case true:
			{
				lpszAllocOut = lpnNode->ToCsv();
				break;
			}

			case false:
			{
				lpszAllocOut = lpnNode->ToConfig();
				break;
			}

		}

		if (NULL == lpszAllocOut)
		{ break; }

		fprintf(fTheFile, "%s\n", lpszAllocOut);
		fflush(fTheFile);
		
		free(lpszAllocOut); 
	} while((lpnNode = Tree::TreeWalk(&gtiIter, false)));


	fclose(fTheFile);
	return true;
}

