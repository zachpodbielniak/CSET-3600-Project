/*

 File: 		Prereqs.h
 Author: 	Zach Podbielniak
 Last Update: 	02/08/2018

 Overview: 	This file will be required for every other file to include.
		It essentially sets everything that needs to be included.

*/


#ifndef PREREQS_H
#define PREREQS_H


#include "Defs.h"
#include "Macros.h"
#include "Typedefs.h"


#include <thread>
#include <vector>
#include <list>
#include <mutex>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <semaphore.h>
#include <unistd.h>

#include <gtk/gtk.h>


/* GTK Type Defs */
typedef gchar 		GCHAR, *LPGSTR;
typedef const gchar 	*LPCGSTR;
typedef gboolean 	GBOOL, *LPGBOOL;
typedef gpointer 	LPGVOID;


typedef pthread_mutex_t MUTEX, *LPMUTEX;
typedef sem_t 		SEMAPHORE, *LPSEMAPHORE;



#endif
