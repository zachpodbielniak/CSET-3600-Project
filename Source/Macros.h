#ifndef MACROS_H
#define MACROS_H


#define UNREFERENCED_PARAMETER(X)			((X) = (X))
#define UNREFERENCED_VARIABLE(X)                        ((X) = (X))

#define BOOL_TO_STR(X)                                  ((0 == (X)) ? "FALSE" : "TRUE")

#endif
