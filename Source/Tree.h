/*
 Author:        Zach Podbielniak
 
 Summary:       This file handles the stuff behind the Tree View.

*/


#ifndef TREE_H
#define TREE_H

#include "Prereqs.h"
#include "Utils.h"
#include "Node/Node.h"
#include "Gui.h"
#include "Logger.h"


typedef struct __TREE   TREE, *LPTREE;


struct __TREE
{
        GBOOL           gbRouter;
        GBOOL           gbSwitch;
        GBOOL           gbServer;
	LPNODE 		lpnDataNode;
        LPTREE          lptChildren;
};


typedef enum __TREE_FORMAT
{
        BRANCH_NAME,
        ROUTER_COLUMN,
        SWITCH_COLUMN,
        SERVER_COLUMN,
	OS_COLUMN,
        FQDN_COLUMN,
	PATH_COLUMN,
	VERS_COLUMN,
        IP_ADDRESS_COLUMN,
        IS_STATIC_LEASE_COLUMN,
	SUBNET_COLUMN,
        MAC_ADDRESS_COLUMN,
	NODE_CLASS,

        TREE_NUM_COLUMNS        /* THIS MUST BE LAST TO WORK! */
} TREE_FORMAT;



typedef enum __HARDWARE_TYPE_FLAGS
{
        HARDWARE_ROUTER         = 1 << 0,
        HARDWARE_SWITCH         = 1 << 1,
        HARDWARE_GATEWAY        = 1 << 2,
        HARDWARE_SERVER         = 1 << 3
} HARDWARE_TYPE_FLAGS;


namespace Tree
{


	BOOL
	InitializeTree(
	        VOID
	);



	BOOL
	InsertIntoTree(
		NODE 		*lpnNode
	);



	GtkTreeView *
	GetTreeViewObject(
		VOID
	);


	NODE *
	TreeWalk(
		GtkTreeIter *gtiIter,
		BOOL bFirst
	);
}

#endif