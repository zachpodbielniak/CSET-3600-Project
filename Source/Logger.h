#ifndef LOGGER_H
#define LOGGER_H


#include "Prereqs.h"


namespace Log 
{


	BOOL
	WriteLog(
		LPCSTR 		lpcszText
	);




	BOOL
	FlushLog(
		VOID
	);


}



#endif
