/*
 Author:        Dave Marley
 Summary:       This file handles the IP address properties module.
*/

#ifndef PROPERTIES_H
#define PROPERTIES_H

#include "Prereqs.h"
#include "Utils.h"

typedef enum __FLAGS_FOR_NODES
{
    IP_FLAG = 1 << 0,
    SUBNET_FLAG = 1 << 1,
    GATE_FLAG = 1 << 2,
    MAC_FLAG = 1 << 3,
    HOST_FLAG = 1 << 4,
    HOSTNAME_FLAG = 1 << 5,
    DDNS_FLAG = 1 << 6,
    DHCP_FLAG = 1 << 7
} FLAGS_FOR_NODES;



/*Flags structure 0x(DHCP,DDNS,HOSTNAME,HOST,MAC,GATE,SUBNET,IP) or 0x11111111 or  (0x00 - 0xFF)
May make longer to include all other variables passed in */

#endif
