/*

 File: 		Typedefs.h
 Author: 	Zach Podbielniak
 Last Update: 	02/05/2018

 Overview: 	This file sets for type definitions for various types. This
		allows us to refer to different types, and have them be
		universal across devices and architectures.

*/



#ifndef TYPEDEFS_H
#define TYPEDEFS_H


#include <stdint.h>


#define GLOBAL_VARIABLE 		static 
#define INTERNAL_OPERATION		static 
#define CONSTANT 			const
#define VIRTUAL 			virtual


#ifndef __TEST_SUITE_BUILD
typedef char				CHAR;
typedef unsigned char 			UCHAR;
typedef uint8_t				BYTE;
typedef bool 				BOOL;
typedef int16_t 			SHORT;
typedef uint16_t 			USHORT;
typedef int32_t 			LONG;
typedef uint32_t			ULONG;
typedef LONG				INT;
typedef ULONG 				UINT;
typedef int64_t 			LONGLONG;
typedef uint64_t 			ULONGLONG;

typedef CHAR 				*LPSTR, **DLPSTR;
typedef CONSTANT CHAR 			*LPCSTR;

typedef void 				VOID;
typedef VOID 				*LPVOID;



#else

#include <Windows.h>

#endif






#endif
