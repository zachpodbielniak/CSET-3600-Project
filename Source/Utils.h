/*
 Author:        Zach Podbielniak

 Summary:       This file has a few functions used to convert
		some types to strings, and strings to other types.

*/


#ifndef UTILS_H
#define UTILS_H


#include "Prereqs.h"


typedef struct __IPV4_ADDRESS
{
	union
	{
		ULONG           ulData;
		BYTE            byData[4];
	};
} IPV4_ADDRESS, *LPIPV4_ADDRESS;




typedef struct __MAC_ADDRESS
{
	union
	{
		ULONGLONG       ullData : 48;
		BYTE            byData[6];
	};
} MAC_ADDRESS, *LPMAC_ADDRESS;


namespace Utils
{


	IPV4_ADDRESS
	CreateIpv4AddressFromString(
		LPCSTR          lpcszIpv4Address
	);




	BOOL 
	Ipv4AddressToString(
		LPIPV4_ADDRESS  lpip4Address,
		LPSTR           lpszBufferOut,
		ULONG           ulBufferSize
	);




	MAC_ADDRESS
	CreateMacAddressFromString(
		LPCSTR          lpcszMacAddress
	);




	BOOL
	MacAddressToString(
		LPMAC_ADDRESS   lpmaMacAddress,
		LPSTR          	lpszBufferOut,
		ULONG           ulBufferSize
	);


}
#endif