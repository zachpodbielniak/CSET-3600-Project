/*

 File: 		Gui.cpp
 Author: 	Zach Podbielniak
 
 Last Update: 	03/26/2018


 Overview: 	This file handles doing all of the GTK magic which
		ties all of the GUI backend stuff together.

*/


#include "Gui.h"
#include "Logger.h"
#include "Node/Node.h"
#include "Tree.h"
#include "Import.h"
#include "Export.h"


/* INTERNAL VARIABLES */
GLOBAL_VARIABLE GtkBuilder *gbMAIN_BUILDER;
GLOBAL_VARIABLE GtkBuilder *gbPROPERTIES_BUILDER;

GLOBAL_VARIABLE GObject *gowMAIN_WINDOW;

GLOBAL_VARIABLE LPSEMAPHORE lpsemProperties;
GLOBAL_VARIABLE LPSEMAPHORE lpsemAbout;
GLOBAL_VARIABLE LPSEMAPHORE lpsemHelp;




/* Call back to handle clearing the tree and starting fresh */
INTERNAL_OPERATION
VOID
__NewCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__NewCallBack() was called\n");
	/* TODO: Clear the Tree */
}




/* Call back to handle file opening */
INTERNAL_OPERATION
VOID
__OpenCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__OpenCallBack() was called\n");

	
	GtkWidget *gwDialog;
	GtkFileChooserAction gfcaAction;
	INT iRes;
	LPSTR lpszFileName;

	gfcaAction = GTK_FILE_CHOOSER_ACTION_OPEN;

	gwDialog = gtk_file_chooser_dialog_new(
		"Open File",
		GTK_WINDOW(gowMAIN_WINDOW),
		gfcaAction,
		"_Cancel",
		GTK_RESPONSE_CANCEL,
		"_Open",
		GTK_RESPONSE_ACCEPT,
		NULL
	);

	iRes = gtk_dialog_run(GTK_DIALOG(gwDialog));
	
	
	if (GTK_RESPONSE_ACCEPT == iRes)
	{
		GtkFileChooser *gfcChooser;

		gfcChooser = GTK_FILE_CHOOSER(gwDialog);
		lpszFileName = gtk_file_chooser_get_filename(gfcChooser);

		/* Handle Calling Import File */
		ImportFile((LPCSTR)lpszFileName);

		g_free(lpszFileName);
	}
	
	gtk_widget_destroy(gwDialog);

}



INTERNAL_OPERATION
VOID
__ExportCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);

	GtkWidget *gwDialog;
	GtkFileChooser *gfcChooser;
	GtkFileChooserAction gfcaAction;
	INT iRes;
	LPSTR lpszFileName;

	gfcaAction = GTK_FILE_CHOOSER_ACTION_SAVE;

	gwDialog = gtk_file_chooser_dialog_new(
		"Save File",
		GTK_WINDOW(gowMAIN_WINDOW),
		gfcaAction,
		"Cancel",
		GTK_RESPONSE_CANCEL,
		"Save",
		GTK_RESPONSE_ACCEPT,
		NULL
	);

	gfcChooser = GTK_FILE_CHOOSER(gwDialog);

	gtk_file_chooser_set_do_overwrite_confirmation(gfcChooser, TRUE);

	gtk_file_chooser_set_current_name(gfcChooser, "Untitled Document");

	iRes = gtk_dialog_run(GTK_DIALOG(gwDialog));

	if (GTK_RESPONSE_ACCEPT == iRes)
	{
		lpszFileName = gtk_file_chooser_get_filename(gfcChooser);

		/* Call Save To File */
		ExportFile(lpszFileName, false);

		/* Free the string, since GTK+ allocates it on the heap. */
		g_free(lpszFileName);
	}

	gtk_widget_destroy(gwDialog);

}




/* Call back for Save and Save As menu items */
INTERNAL_OPERATION
VOID
__SaveCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);

	GtkWidget *gwDialog;
	GtkFileChooser *gfcChooser;
	GtkFileChooserAction gfcaAction;
	INT iRes;
	LPSTR lpszFileName;

	gfcaAction = GTK_FILE_CHOOSER_ACTION_SAVE;

	gwDialog = gtk_file_chooser_dialog_new(
		"Save File",
		GTK_WINDOW(gowMAIN_WINDOW),
		gfcaAction,
		"Cancel",
		GTK_RESPONSE_CANCEL,
		"Save",
		GTK_RESPONSE_ACCEPT,
		NULL
	);

	gfcChooser = GTK_FILE_CHOOSER(gwDialog);

	gtk_file_chooser_set_do_overwrite_confirmation(gfcChooser, TRUE);

	gtk_file_chooser_set_current_name(gfcChooser, "Untitled Document");

	iRes = gtk_dialog_run(GTK_DIALOG(gwDialog));

	if (GTK_RESPONSE_ACCEPT == iRes)
	{
		lpszFileName = gtk_file_chooser_get_filename(gfcChooser);

		/* Call Save To File */
		ExportFile(lpszFileName, true);

		/* Free the string, since GTK+ allocates it on the heap. */
		g_free(lpszFileName);
	}

	gtk_widget_destroy(gwDialog);

}




/* Call back to exit the program */
INTERNAL_OPERATION
VOID
__ExitCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData 
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__ExitCallBack() was called! SYSTEM WILL NOW EXIT!\n");

		

	/* Handle any clean up code here */

	Log::FlushLog();

	/* GTK handles clean up of all GTK-based allocations */
	gtk_main_quit();
}




/* Call back for Cut */
/* TODO: Not sure if we're going to utilize this yet? */
INTERNAL_OPERATION
VOID
__CutCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__CutCallBack() was called\n");
}




/* Call back for Copy */
/* TODO: Not sure if we're going to utilize this yet? */
INTERNAL_OPERATION
VOID
__CopyCallBack(	
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__CopyCallBack() was called\n");
}




/* Call back for Paste */
/* TODO: Not sure if we're going to utilize this yet? */
INTERNAL_OPERATION
VOID
__PasteCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__PasteCallBack() was called\n");
}




/* Call back to handle deleting selected tree-leaf / node */
INTERNAL_OPERATION
VOID
__DeleteCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__DeleteCallBack() was called\n");
	/* TODO: Get current tree selection, and delete it */
}




/* Call back to show About Window */
INTERNAL_OPERATION
VOID
__AboutCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__AboutCallBack() was called\n");
	Gui::ShowAboutWindow();
}




/* Call back to show Help Window */
INTERNAL_OPERATION
VOID
__HelpMeCallBack(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__HelpMeCallBack() was called\n");
	Gui::ShowHelpWindow();
}




/* Call back to exit the About Window */
INTERNAL_OPERATION
VOID
__AboutWindowExit(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData 
){
	UNREFERENCED_PARAMETER(gwWidget);
	Log::WriteLog("__AboutWindowExit() was called, closing the about window.\n");
	gtk_widget_destroy(GTK_WIDGET(lpgData));
	sem_post(lpsemAbout);
}




/* Call back to exit the Help Window */
INTERNAL_OPERATION
VOID
__HelpWindowExit(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData 
){
	UNREFERENCED_PARAMETER(gwWidget);
	Log::WriteLog("__HelpWindowExit() was called, closing the help window.\n");
	gtk_widget_destroy(GTK_WIDGET(lpgData));
	sem_post(lpsemHelp);
}
















/* Function to remove node / leaf */
INTERNAL_OPERATION
VOID
__RemoveCallback(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__RemoveCallback() was called.\n");
	/* TODO: Get pointer to tree and remove selected data */
}


/*
	PROPERTIES WINDOW SECTION
*/




INTERNAL_OPERATION
VOID
__PropertiesWindowExitCallback(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData 
){
	UNREFERENCED_PARAMETER(gwWidget);
	Log::WriteLog("__PropertiesWindowExitCallback() was called, closing the properties window.\n");
	gtk_widget_destroy(GTK_WIDGET(lpgData));

	/* Release the semaphore */
	sem_post(lpsemProperties);
}




INTERNAL_OPERATION
VOID
__PropertiesCancelCallback(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	Log::WriteLog("__PropertiesCancelCallback() was called, cancelling this operation.\n");
	
	/* We just need to call __PropertiesWindowExitCallback() to handle this. */
	__PropertiesWindowExitCallback(gwWidget, lpgData);
}




/* Callback for when "Ok" is hit on Properties Window */
INTERNAL_OPERATION
VOID
__PropertiesOkCallback(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	GObject *goswIpAuto, *goswDnsAuto;
	GObject *gotxtIpAddress, *gotxtSubnet, *gotxtGateway, *gotxtMacAddress;
	GObject *gotxtName, *gotxtHostName, *gotxtDnsOne, *gotxtDnsTwo;
	LPCSTR lpcszIpAddress, lpcszSubnet, lpcszGateway, lpcszMacAddress;
	LPCSTR lpcszName, lpcszHostName, lpcszDnsOne, lpcszDnsTwo;
	IPV4_ADDRESS ip4IpAddress, ip4Subnet, ip4Gateway, ip4DnsOne, ip4DnsTwo;
	MAC_ADDRESS maMacAddress;
	BOOL bAutoIp, bAutoDns;
	CHAR szBuffer[0x200];

	Log::WriteLog("__PropertiesOkCallback() was called, saving these details...\n");


	/* Get the reference for both switches, and get their 'state' */
	goswIpAuto = gtk_builder_get_object(gbPROPERTIES_BUILDER, "swIpAuto");
	goswDnsAuto = gtk_builder_get_object(gbPROPERTIES_BUILDER, "swDnsAuto");
	bAutoIp = gtk_switch_get_active(GTK_SWITCH(goswIpAuto));
	bAutoDns = gtk_switch_get_active(GTK_SWITCH(goswDnsAuto));

	/* Lets log their state */
	snprintf(szBuffer,
		0x200,
		"The status of swIpAuto is %s\nThe status of swDnsAuto is %s\n",
		BOOL_TO_STR(bAutoIp),
		BOOL_TO_STR(bAutoDns)
	);
	Log::WriteLog(szBuffer);	


	/* Lets get the text content */
	gotxtIpAddress = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtIp");
	lpcszIpAddress = gtk_entry_get_text(GTK_ENTRY(gotxtIpAddress));

	gotxtSubnet = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtSubnet");
	lpcszSubnet = gtk_entry_get_text(GTK_ENTRY(gotxtSubnet));

	gotxtGateway = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtGateway");
	lpcszGateway = gtk_entry_get_text(GTK_ENTRY(gotxtGateway));
	
	gotxtMacAddress = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtMacAddress");
	lpcszMacAddress = gtk_entry_get_text(GTK_ENTRY(gotxtMacAddress));

	gotxtName = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtName");
	lpcszName = gtk_entry_get_text(GTK_ENTRY(gotxtName));

	gotxtHostName = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtHostName");
	lpcszHostName = gtk_entry_get_text(GTK_ENTRY(gotxtHostName));

	gotxtDnsOne = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtDnsOne");
	lpcszDnsOne = gtk_entry_get_text(GTK_ENTRY(gotxtDnsOne));

	gotxtDnsTwo = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtDnsTwo");
	lpcszDnsTwo = gtk_entry_get_text(GTK_ENTRY(gotxtDnsTwo));

	/* Lets log all that info */
	snprintf(
		szBuffer,
		0x200,
		"Name:\t\t%s\nHostname:\t%s\nIP:\t\t%s\nSubnet:\t\t%s\nGateway:\t%s\nMAC:\t\t%s\nDNS1:\t\t%s\nDNS2:\t\t%s\n",
		lpcszName,
		lpcszHostName,
		lpcszIpAddress,
		lpcszSubnet,
		lpcszGateway,
		lpcszMacAddress,
		lpcszDnsOne,
		lpcszDnsTwo
	);
	Log::WriteLog(szBuffer);



	/* Make IPV4 Addresses, and Mac Address */
	ip4IpAddress = Utils::CreateIpv4AddressFromString(lpcszIpAddress);
	ip4Subnet = Utils::CreateIpv4AddressFromString(lpcszSubnet);
	ip4Gateway = Utils::CreateIpv4AddressFromString(lpcszGateway);
	ip4DnsOne = Utils::CreateIpv4AddressFromString(lpcszDnsOne);
	ip4DnsTwo = Utils::CreateIpv4AddressFromString(lpcszDnsTwo);
	maMacAddress = Utils::CreateMacAddressFromString(lpcszMacAddress);



	/* TODO: 			*/
	/* Get Tree from gbMAIN_BUILDER */	
	/* Insert new data into tree 	*/



	/* Finally close this window */
	__PropertiesWindowExitCallback(gwWidget, lpgData);
}




/* Callback to handle changes on swIpAuto */
INTERNAL_OPERATION
VOID
__PropertiesIpSwitchCallback(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__PropertiesIpSwitchCallback() was called\n");
}








/* Function to add a new node / leaf */
INTERNAL_OPERATION
VOID
__AddIntoTreeCallback(
	GtkWidget 	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	Log::WriteLog("__AddCallback() was called.\n");


	/*GObject *goswIpAuto, *goswDnsAuto;*/
	GObject *gotxtIpAddress, *gotxtSubnet, *gotxtMacAddress;
	GObject *gotxtName, *gotxtHostName, *gotxtPath, *gotxtVersion;
	LPCSTR lpcszIpAddress, lpcszSubnet, lpcszMacAddress;
	LPCSTR lpcszName, lpcszHostName, lpcszPath, lpcszVersion;
	IPV4_ADDRESS ip4IpAddress, ip4Subnet;
	MAC_ADDRESS maMacAddress;
	/*BOOL bAutoIp, bAutoDns;*/
	CHAR szBuffer[0x200];
	NODE *lpnNode;


	/* Lets get the text content */
	gotxtIpAddress = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtIp");
	lpcszIpAddress = gtk_entry_get_text(GTK_ENTRY(gotxtIpAddress));

	gotxtSubnet = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtSubnet");
	lpcszSubnet = gtk_entry_get_text(GTK_ENTRY(gotxtSubnet));

	gotxtMacAddress = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtMacAddress");
	lpcszMacAddress = gtk_entry_get_text(GTK_ENTRY(gotxtMacAddress));

	gotxtName = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtName");
	lpcszName = gtk_entry_get_text(GTK_ENTRY(gotxtName));

	gotxtHostName = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtHostName");
	lpcszHostName = gtk_entry_get_text(GTK_ENTRY(gotxtHostName));

	gotxtPath = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtPath");
	lpcszPath = gtk_entry_get_text(GTK_ENTRY(gotxtPath));

	gotxtVersion = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtVersion");
	lpcszVersion = gtk_entry_get_text(GTK_ENTRY(gotxtVersion));

	/* Lets log all that info */
	snprintf(
		szBuffer,
		0x200,
		"Name:\t\t%s\nHostname:\t%s\nIP:\t\t%s\nSubnet:\t\t%s\nMAC:\t\t%s\nPath:\t\t%s\nVersion:\t%s\n",
		lpcszName,
		lpcszHostName,
		lpcszIpAddress,
		lpcszSubnet,
		lpcszMacAddress,
		lpcszPath,
		lpcszVersion
	);
	Log::WriteLog(szBuffer);



	/* Make IPV4 Addresses, and Mac Address */
	ip4IpAddress = Utils::CreateIpv4AddressFromString(lpcszIpAddress);
	ip4Subnet = Utils::CreateIpv4AddressFromString(lpcszSubnet);
	maMacAddress = Utils::CreateMacAddressFromString(lpcszMacAddress);


	/* TODO: Fix this */
	lpnNode = new NODE(
		(LPSTR)lpcszName,
		(LPSTR)lpcszHostName,
		(LPSTR)lpcszPath,
		(LPSTR)lpcszVersion,
		NODE_OS_LINUX,
		NODE_TYPE_HOST,
		ip4IpAddress,
		ip4Subnet,
		maMacAddress,
		FALSE
	);

	Tree::InsertIntoTree(lpnNode);
	


	/* Finally close this window */
	__PropertiesWindowExitCallback(gwWidget, lpgData);
}




INTERNAL_OPERATION
VOID
__AddCallback(
	GtkWidget  	*gwWidget,
	LPGVOID 	lpgData
){
	UNREFERENCED_PARAMETER(gwWidget);
	UNREFERENCED_PARAMETER(lpgData);
	GtkBuilder *gbBuilder;
	GObject *gowPropertiesWindow;
	GObject *gotbtnExit, *gobtnCancel, *gobtnOk;
	GObject *goswIpAuto;

	/* Acquire Properties Semaphore, so only one window can be open at at time */
	if (0 != sem_trywait(lpsemProperties))
	{ return; }
	
	/* Construct a GtkBuilder instance and load our UI description */

	gbBuilder = gtk_builder_new();
	gbPROPERTIES_BUILDER = gbBuilder;
	gtk_builder_add_from_file(gbBuilder, PROP_WINDOW_UI, NULL);

	gowPropertiesWindow = gtk_builder_get_object(gbBuilder, "wPropertiesWindow");


	gotbtnExit = gtk_builder_get_object(gbBuilder, "tbtnExit");
	g_signal_connect(
			gotbtnExit, 
			"clicked", 
			G_CALLBACK(__PropertiesWindowExitCallback), 
			gowPropertiesWindow
	);

	gobtnCancel = gtk_builder_get_object(gbBuilder, "btnCancel");
	g_signal_connect(
			gobtnCancel, 
			"clicked", 
			G_CALLBACK(__PropertiesCancelCallback), 
			gowPropertiesWindow
	); /* Pass in the reference to the window object */

	gobtnOk = gtk_builder_get_object(gbBuilder, "btnOk");
	g_signal_connect(
			gobtnOk, 
			"clicked", 
			G_CALLBACK(__AddIntoTreeCallback), 
			gowPropertiesWindow
	);


	/* Switches */
	goswIpAuto = gtk_builder_get_object(gbBuilder, "swIpAuto");
	g_signal_connect(
			goswIpAuto, 
			"state-set", 	/* Signal Type For Swap */
			G_CALLBACK(__PropertiesIpSwitchCallback), 
			NULL
	);

}


GtkWindow *
Gui::ShowPropertiesWindow(
	LPVOID 		lpReserved
){
	UNREFERENCED_PARAMETER(lpReserved);
	GtkBuilder *gbBuilder;
	GObject *gowPropertiesWindow;
	GObject *gotbtnExit, *gobtnCancel, *gobtnOk;
	GObject *goswIpAuto;
	GObject *gotxtIpAddress, *gotxtSubnet, *gotxtGateway, *gotxtMacAddress;
	GObject *gotxtName, *gotxtHostName, *gotxtPath, *gotxtVersion;
	GtkTreeSelection *gtsSelect;
	GtkTreeModel *gtmModel;
	GtkTreeIter gtiIter;
	NODE *lpnNode;
	LPNODE_CORE lpntCore;
	CHAR szBuffer[0x100];
	LPSTR lpszOut;

	/* Acquire Properties Semaphore, so only one window can be open at at time */
	if (0 != sem_trywait(lpsemProperties))
	{ return NULL; }
	
	/* Construct a GtkBuilder instance and load our UI description */

	gbBuilder = gtk_builder_new();
	gbPROPERTIES_BUILDER = gbBuilder;
	gtk_builder_add_from_file(gbBuilder, PROP_WINDOW_UI, NULL);

	gowPropertiesWindow = gtk_builder_get_object(gbBuilder, "wPropertiesWindow");


	gotbtnExit = gtk_builder_get_object(gbBuilder, "tbtnExit");
	g_signal_connect(
			gotbtnExit, 
			"clicked", 
			G_CALLBACK(__PropertiesWindowExitCallback), 
			gowPropertiesWindow
	);

	gobtnCancel = gtk_builder_get_object(gbBuilder, "btnCancel");
	g_signal_connect(
			gobtnCancel, 
			"clicked", 
			G_CALLBACK(__PropertiesCancelCallback), 
			gowPropertiesWindow
	); /* Pass in the reference to the window object */

	gobtnOk = gtk_builder_get_object(gbBuilder, "btnOk");
	g_signal_connect(
			gobtnOk, 
			"clicked", 
			G_CALLBACK(__PropertiesOkCallback), 
			gowPropertiesWindow
	);


	/* Switches */
	goswIpAuto = gtk_builder_get_object(gbBuilder, "swIpAuto");
	g_signal_connect(
			goswIpAuto, 
			"state-set", 	/* Signal Type For Swap */
			G_CALLBACK(__PropertiesIpSwitchCallback), 
			NULL
	);

	gtsSelect = gtk_tree_view_get_selection(
		Tree::GetTreeViewObject()
	);

	if (NULL == gtsSelect)
	{ return NULL; }

	gtk_tree_selection_get_selected(
		gtsSelect,
		&gtmModel,
		&gtiIter
	);


	/* Retreive the NODE Object. */
	gtk_tree_model_get(
		gtmModel,
		&gtiIter,
		NODE_CLASS, &lpnNode,
		-1
	);

	lpntCore = lpnNode->GetNodeCore();

	memset(szBuffer, 0x00, sizeof(szBuffer));

	/* Lets set the text content */
	Utils::Ipv4AddressToString(&(lpntCore->ip4Address), szBuffer, sizeof(szBuffer));
	gotxtIpAddress = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtIp");
	gtk_entry_set_text(GTK_ENTRY(gotxtIpAddress), szBuffer);
	
	Utils::Ipv4AddressToString(&(lpntCore->ip4Subnet), szBuffer, sizeof(szBuffer));
	gotxtSubnet = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtSubnet");
	gtk_entry_set_text(GTK_ENTRY(gotxtSubnet), szBuffer);


	gotxtGateway = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtGateway");
	gtk_entry_set_text(GTK_ENTRY(gotxtGateway), "");
	
	Utils::MacAddressToString(&(lpntCore->maMacAddress), szBuffer, sizeof(szBuffer));
	gotxtMacAddress = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtMacAddress");
	gtk_entry_set_text(GTK_ENTRY(gotxtMacAddress), szBuffer);

	gtk_tree_model_get(
		gtmModel,
		&gtiIter,
		BRANCH_NAME, &lpszOut,
		-1
	);
	strncpy(szBuffer, lpszOut, sizeof(szBuffer));
	g_free(lpszOut);
	gotxtName = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtName");
	gtk_entry_set_text(GTK_ENTRY(gotxtName), szBuffer);

	gtk_tree_model_get(
		gtmModel,
		&gtiIter,
		FQDN_COLUMN, &lpszOut,
		-1
	);
	strncpy(szBuffer, lpszOut, sizeof(szBuffer));
	g_free(lpszOut);
	gotxtHostName = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtHostName");
	gtk_entry_set_text(GTK_ENTRY(gotxtHostName), szBuffer);


	gtk_tree_model_get(
		gtmModel,
		&gtiIter,
		PATH_COLUMN, &lpszOut,
		-1
	);
	strncpy(szBuffer, lpszOut, sizeof(szBuffer));
	g_free(lpszOut);
	gotxtPath = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtPath");
	gtk_entry_set_text(GTK_ENTRY(gotxtPath), szBuffer);


	gtk_tree_model_get(
		gtmModel,
		&gtiIter,
		VERS_COLUMN, &lpszOut,
		-1
	);
	strncpy(szBuffer, lpszOut, sizeof(szBuffer));
	g_free(lpszOut);
	gotxtVersion = gtk_builder_get_object(gbPROPERTIES_BUILDER, "txtVersion");
	gtk_entry_set_text(GTK_ENTRY(gotxtVersion), szBuffer);
	




	return GTK_WINDOW(gowPropertiesWindow);
}




/* Function to show the about window */
VOID
Gui::ShowAboutWindow(
	VOID
){
	GtkBuilder *gbBuilder;
	GObject *gowAboutWindow;
	GObject *gobtnExit, *gotbtnExit;

	/* Acquire About Window Semaphore, so only one window can be open at at time */
	if (0 != sem_trywait(lpsemAbout))
	{ return; }


	gbBuilder = gtk_builder_new();
	gtk_builder_add_from_file(gbBuilder, ABOUT_WINDOW_UI, NULL);

	gowAboutWindow = gtk_builder_get_object(gbBuilder, "wAboutWindow");

	gobtnExit = gtk_builder_get_object(gbBuilder, "btnAboutExit");
	g_signal_connect(
			gobtnExit, 
			"clicked", 
			G_CALLBACK(__AboutWindowExit), 
			gowAboutWindow
	);	

	gotbtnExit = gtk_builder_get_object(gbBuilder, "tbtnExit");
	g_signal_connect(
			gotbtnExit, 
			"clicked", 
			G_CALLBACK(__AboutWindowExit), 
			gowAboutWindow
	);
}




/* Function to show the help window */
VOID
Gui::ShowHelpWindow(
	VOID
){
	GtkBuilder *gbBuilder;
	GObject *gowHelpWindow;
	GObject *gobtnExit, *gotbtnExit;
	CHAR szBuffer[0x100];

	/* Acquire Help Window Semaphore, so only one window can be open at at time */
	if (0 != sem_trywait(lpsemHelp))
	{ return; }


	gbBuilder = gtk_builder_new();
	gtk_builder_add_from_file(gbBuilder, HELP_WINDOW_UI, NULL);

	gowHelpWindow = gtk_builder_get_object(gbBuilder, "wHelpWindow");

	gobtnExit = gtk_builder_get_object(gbBuilder, "btnHelpExit");
	g_signal_connect(
			gobtnExit, 
			"clicked", 
			G_CALLBACK(__HelpWindowExit),
			gowHelpWindow
	);
	
	gotbtnExit = gtk_builder_get_object(gbBuilder, "tbtnExit");
	g_signal_connect(
			gotbtnExit, 
			"clicked", 
			G_CALLBACK(__HelpWindowExit), 
			gowHelpWindow
	);


	gtk_window_set_title(GTK_WINDOW(gowHelpWindow), HELP_WINDOW_TITLE);
	snprintf(
		szBuffer,
		0x100U,
		"Set title for gwHelpWindow (%p) to %s\n", 
		GTK_WINDOW(gowHelpWindow), 
		HELP_WINDOW_TITLE
	);
	Log::WriteLog(szBuffer);
}




/* Function that shows the main window */
BOOL
Gui::CreateMainWindow(
	VOID
){
	GtkBuilder *gbBuilder;
	GtkWindow *gwMainWindow;
	GdkGeometry ggmMainWindowProperties;
	GObject *gowMainWindow, *gomiNew, *gomiOpen;
	GObject *gomiSave, *gomiSaveAs, *gomiExit;
	GObject *gomiCut, *gomiCopy, *gomiPaste, *gomiDelete;
	GObject *gomiAbout, *gomiHelpMe;
	GObject *gtbtnOpen, *gtbtnSave, *gtbtnSaveAs, *gtbtnExit;
	GObject *gtbtnAdd, *gtbtnRemove, *gtbtnProperties, *gtbtnExport;
	CHAR szMessageBuffer[0x100];

	/* Allocate if need be, the properties semaphore */
	if (NULL == lpsemProperties)
	{
		lpsemProperties = (LPSEMAPHORE)malloc(sizeof(SEMAPHORE));
		if (NULL == lpsemProperties)
		{
			Log::WriteLog("FATAL!!!! FAILED TO ALLOCATE PROPERTIES SEMAPHORE!\n");
			exit(1);
		}
		sem_init(lpsemProperties, 0, 0x01U);
	}

	/* Allocate if need be, the help semaphore */
	if (NULL == lpsemHelp)
	{
		lpsemHelp = (LPSEMAPHORE)malloc(sizeof(SEMAPHORE));
		if (NULL == lpsemHelp)
		{
			Log::WriteLog("FATAL!!!! FAILED TO ALLOCATE THE HELP SEMAPHORE!\n");
			exit(1);
		}
		sem_init(lpsemHelp, 0, 0x01U);
	}
	
	/* Allocate if need be, the about semaphore */
	if (NULL == lpsemAbout)
	{
		lpsemAbout = (LPSEMAPHORE)malloc(sizeof(SEMAPHORE));
		if (NULL == lpsemAbout)
		{
			Log::WriteLog("FATAL!!!! FAILED TO ALLOCATE THE ABOUT SEMAPHORE!\n");
			exit(1);
		}
		sem_init(lpsemAbout, 0, 0x01U);
	}


	/* Construct a GtkBuilder instance and load our UI description */
	gbBuilder = gtk_builder_new();
	gbMAIN_BUILDER = gbBuilder;
	gtk_builder_add_from_file(gbBuilder, MAIN_WINDOW_UI, NULL);

 	/* Connect signal handlers to the constructed widgets. */

	gowMainWindow = gtk_builder_get_object(gbBuilder, "mMainWindow");
	gowMAIN_WINDOW = gowMainWindow;
	gwMainWindow = GTK_WINDOW(gowMainWindow);
	g_signal_connect(
			gwMainWindow, 
			"destroy", 
			G_CALLBACK(__ExitCallBack), 
			NULL
	);

	gomiNew = gtk_builder_get_object(gbBuilder, "miNew");
	g_signal_connect(
			gomiNew, 
			"activate", 
			G_CALLBACK(__NewCallBack), 
			NULL
	);

	gomiOpen = gtk_builder_get_object(gbBuilder, "miOpen");
	g_signal_connect(
			gomiOpen, 
			"activate", 
			G_CALLBACK(__OpenCallBack), 
			NULL
	);

	gomiSave = gtk_builder_get_object(gbBuilder, "miSave");
	g_signal_connect(
			gomiSave, 
			"activate", 
			G_CALLBACK(__SaveCallBack), 
			NULL
	);

	gomiSaveAs = gtk_builder_get_object(gbBuilder, "miSaveAs");
	g_signal_connect(
			gomiSaveAs, 
			"activate", 
			G_CALLBACK(__SaveCallBack), 
			(LPGVOID)(size_t)0x01
	);

	gomiExit = gtk_builder_get_object(gbBuilder, "miExit");
	g_signal_connect(
			gomiExit, 
			"activate", 
			G_CALLBACK(__ExitCallBack), 
			NULL
	);

	gomiCut = gtk_builder_get_object(gbBuilder, "miCut");
	g_signal_connect(
			gomiCut, 
			"activate", 
			G_CALLBACK(__CutCallBack), 
			NULL
	);

	gomiCopy = gtk_builder_get_object(gbBuilder, "miCopy");
	g_signal_connect(
			gomiCopy, 
			"activate", 
			G_CALLBACK(__CopyCallBack), 
			NULL
	);
	
	gomiPaste = gtk_builder_get_object(gbBuilder, "miPaste");
	g_signal_connect(
			gomiPaste, 
			"activate", 
			G_CALLBACK(__PasteCallBack), 
			NULL
	);

	gomiDelete = gtk_builder_get_object(gbBuilder, "miDelete");
	g_signal_connect(
			gomiDelete, 
			"activate", 
			G_CALLBACK(__DeleteCallBack), 
			NULL
	);

	gomiAbout = gtk_builder_get_object(gbBuilder, "miAbout");
	g_signal_connect(
			gomiAbout, 
			"activate", 
			G_CALLBACK(__AboutCallBack), 
			NULL
	);

	gomiHelpMe = gtk_builder_get_object(gbBuilder, "miHelpMe");
	g_signal_connect(
			gomiHelpMe, 
			"activate", 
			G_CALLBACK(__HelpMeCallBack), 
			NULL
	);

	/* Tool Bar Buttons */
	gtbtnOpen = gtk_builder_get_object(gbBuilder, "tbtnOpen");
	g_signal_connect(
			gtbtnOpen, 
			"clicked", 
			G_CALLBACK(__OpenCallBack), 
			NULL
	);

	gtbtnSave = gtk_builder_get_object(gbBuilder, "tbtnSave");
	g_signal_connect(
			gtbtnSave, 
			"clicked", 
			G_CALLBACK(__SaveCallBack), 
			NULL
	);

	gtbtnSaveAs = gtk_builder_get_object(gbBuilder, "tbtnSaveAs");
	g_signal_connect(
			gtbtnSaveAs, 
			"clicked", 
			G_CALLBACK(__SaveCallBack), 
			(LPGVOID)(size_t)0x01
	);

	gtbtnExit = gtk_builder_get_object(gbBuilder, "tbtnExit");
	g_signal_connect(
			gtbtnExit, 
			"clicked", 
			G_CALLBACK(__ExitCallBack), 
			NULL
	);

	/* Gui::ShowPropertiesWindow() is not a proper G_CALLBACK function, need to change */
	gtbtnProperties = gtk_builder_get_object(gbBuilder, "tbtnProperties");
	g_signal_connect(
			gtbtnProperties, 
			"clicked", 
			G_CALLBACK(Gui::ShowPropertiesWindow), 
			NULL
	); 

	/* Add and remove buttons */
	gtbtnAdd = gtk_builder_get_object(gbBuilder, "tbtnAdd");
	g_signal_connect(
			gtbtnAdd, 
			"clicked", 
			G_CALLBACK(__AddCallback), 
			NULL
	);

	gtbtnRemove = gtk_builder_get_object(gbBuilder, "tbtnRemove");
	g_signal_connect(
			gtbtnRemove, 
			"clicked", 
			G_CALLBACK(__RemoveCallback), 
			NULL
	);

	gtbtnExport = gtk_builder_get_object(gbBuilder, "tbtnExport");
	g_signal_connect(
			gtbtnExport,
			"clicked",
			G_CALLBACK(__ExportCallBack),
			NULL
	);




	snprintf(
		szMessageBuffer, 
		0x100,
		"Successfully created Main Window at %p\n", 
		gwMainWindow
	);
	Log::WriteLog(szMessageBuffer);



	/* Set Window title and log it */
	gtk_window_set_title(gwMainWindow, MAIN_WINDOW_TITLE);
	snprintf(
		szMessageBuffer, 
		0x100,
		"Set title for gwMainWindow (%p) to %s\n", 
		gwMainWindow, 
		MAIN_WINDOW_TITLE
	);
	Log::WriteLog(szMessageBuffer);



	/* Set Window default size and log it */
	ggmMainWindowProperties.min_width = MAIN_WINDOW_WIDTH;
	ggmMainWindowProperties.min_height = MAIN_WINDOW_HEIGHT;
	gtk_window_set_geometry_hints(gwMainWindow, NULL, &ggmMainWindowProperties, GDK_HINT_MIN_SIZE);
	snprintf(
		szMessageBuffer, 
		0x100,
		"Setting default width and height for gwMainWindow (%p) to %u, %u\n", 
		gwMainWindow, 
		(ULONG)MAIN_WINDOW_WIDTH, 
		(ULONG)MAIN_WINDOW_HEIGHT
	);
	Log::WriteLog(szMessageBuffer);

	Log::FlushLog();


	return true;
}




GtkBuilder *
Gui::GetMainWindowBuilder(
	VOID
){ return gbMAIN_BUILDER; }