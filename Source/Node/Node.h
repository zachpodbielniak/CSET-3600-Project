#include "../Prereqs.h"
#include "../Utils.h"


#ifndef NODE_H
#define NODE_H


#define NODE_CSV_FORMAT		"%s , %s , %s , %s , %d , %d , %s , %s , %s , %hhu\n"
#define NODE_CSV_FORMAT_NO_LINE	"%s , %s , %s , %s , %d , %d , %s , %s , %s , %hhu"




typedef enum __NODE_TYPE
{
	NODE_TYPE_SWITCH        = 1,
	NODE_TYPE_ROUTER,
	NODE_TYPE_HOST
} NODE_TYPE;


typedef enum __NODE_OS
{
	NODE_OS_LINUX 		= 1,
	NODE_OS_WINDOWS 	= 2,
	NODE_OS_OTHER 		= 3
} NODE_OS;



#define NODE_OS_TO_STR(TYPE)					\
	(NODE_OS_LINUX == TYPE) ? "LINUX" : 			\
		(NODE_OS_WINDOWS == TYPE) ? "WINDOWS" : "OTHER" 

#define NODE_TYPE_TO_STR(TYPE)					\
	(NODE_TYPE_HOST == TYPE) ? "vm" :			\
		(NODE_TYPE_ROUTER == TYPE) ? "hub" : "hub"	\


typedef struct __NODE_CORE
{
	CHAR 			szName[0x100];
	CHAR 			szFqdn[0x100];
	CHAR 			szPath[0x100];
	CHAR 			szVers[0x10];
	NODE_OS			nosOs;
	NODE_TYPE               ntType;
	IPV4_ADDRESS 		ip4Address;
	IPV4_ADDRESS 		ip4Subnet;
	MAC_ADDRESS 		maMacAddress;
	BOOL 			bIsDynamicLease;

} NODE_CORE, *LPNODE_CORE;




class NODE
{
protected: 
	NODE_CORE 	ncCore;
	GtkTreeIter 	gtiIter;
public:
 	NODE(
		LPSTR 		lpszName,
		LPSTR 		lpszFqdn,
		LPSTR 		lpszPath,
		LPSTR 		lpszVers,
		NODE_OS 	nosOs,
		NODE_TYPE 	ntType,
		IPV4_ADDRESS 	ip4Address,
		IPV4_ADDRESS 	ip4Subnet,
		MAC_ADDRESS 	maMacAddress,
		BOOL 		bIsDynamicLease
	);


	~NODE();



	/* The returned string MUST be freed! */
	LPSTR
	ToCsv();


	/* The returned string MUST be freed! */
	LPSTR 
	ToConfig();


	LPNODE_CORE
	GetNodeCore(
		VOID
	);


	BOOL
	SetIter(
		GtkTreeIter gtiIter
	);
};


typedef NODE 		*LPNODE;

#endif