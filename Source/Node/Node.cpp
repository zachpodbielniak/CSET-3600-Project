#include "Node.h"




 NODE::NODE(
	LPSTR 		lpszName,
	LPSTR 		lpszFqdn,
	LPSTR 		lpszPath,
	LPSTR 		lpszVers,
	NODE_OS 	nosOs,
	NODE_TYPE 	ntType,
	IPV4_ADDRESS 	ip4Address,
	IPV4_ADDRESS 	ip4Subnet,
	MAC_ADDRESS 	maMacAddress,
	BOOL 		bIsDynamicLease
){
	strncpy(
		this->ncCore.szName,
		lpszName,
		0x100
	);

	strncpy(
		this->ncCore.szFqdn,
		lpszFqdn,
		0x100
	);

	strncpy(
		this->ncCore.szPath,
		lpszPath,
		0x100
	);

	strncpy(
		this->ncCore.szVers,
		lpszVers,
		0x10
	);

	this->ncCore.nosOs = nosOs;
	this->ncCore.ntType = ntType;
	this->ncCore.ip4Address = ip4Address;
	this->ncCore.ip4Subnet = ip4Subnet;
	this->ncCore.maMacAddress = maMacAddress;
	this->ncCore.bIsDynamicLease = bIsDynamicLease;
}


NODE::~NODE(
	VOID
){}



/* TODO: Update the following with config friendly dumping */

LPSTR 
NODE::ToCsv(
	VOID
){
	CHAR szBuffer[0x1000];
	CHAR szIpAddress[0x40];
	CHAR szSubnet[0x40];
	CHAR szMacAddress[0x40];
	LPSTR lpszOut;


	Utils::Ipv4AddressToString(&(this->ncCore.ip4Address), szIpAddress, sizeof(szIpAddress));
	Utils::Ipv4AddressToString(&(this->ncCore.ip4Subnet), szSubnet, sizeof(szSubnet));
	Utils::MacAddressToString(&(this->ncCore.maMacAddress), szMacAddress, sizeof(szMacAddress));
	


	sprintf(szBuffer, 
		NODE_CSV_FORMAT_NO_LINE,
		this->ncCore.szName,
		this->ncCore.szFqdn,
		this->ncCore.szPath,
		this->ncCore.szVers,
		this->ncCore.nosOs,
		this->ncCore.ntType,
		szIpAddress,
		szSubnet,
		szMacAddress,
		(BYTE)this->ncCore.bIsDynamicLease
	);

	lpszOut = (LPSTR)malloc(sizeof(CHAR) * strlen(szBuffer) + 2);
	strncpy(lpszOut, szBuffer, sizeof(CHAR) * strlen(szBuffer) + 2);

	return lpszOut;
}




/* The returned string MUST be freed! */
LPSTR 
NODE::ToConfig(
	VOID
){
	CHAR szBuffer[0x1000];
	CHAR szTemp[0x200];
	CHAR szIpAddress[0x40];
	CHAR szSubnet[0x40];
	LPSTR lpszAllocOut;

	memset(szBuffer, 0x00, sizeof(szBuffer));
	memset(szTemp, 0x00, sizeof(szTemp));

	Utils::Ipv4AddressToString(&(this->ncCore.ip4Address), szIpAddress, sizeof(szIpAddress));
	Utils::Ipv4AddressToString(&(this->ncCore.ip4Subnet), szSubnet, sizeof(szSubnet));


	/* First line */
	snprintf(
		szTemp,
		sizeof(szTemp),
		"%s %s {\n",
		NODE_TYPE_TO_STR(this->ncCore.ntType),
		this->ncCore.szName
	);
	strncat(szBuffer, szTemp, sizeof(szBuffer));

	/* OS + Vers*/
	snprintf(
		szTemp,
		sizeof(szTemp),
		"\tos : %s\n\tver : \"%s\"\n",
		NODE_OS_TO_STR(this->ncCore.nosOs),
		this->ncCore.szVers
	);
	strncat(szBuffer, szTemp, sizeof(szBuffer));

	/* Src? */
	if (0 != ncCore.szPath[0])
	{
		snprintf(
			szTemp,
			sizeof(szTemp),
			"\tsrc : %s\n",
			this->ncCore.szPath
		);
		strncat(szBuffer, szTemp, sizeof(szBuffer));
	}
	
	/* Ip and Subnet */
	snprintf(
		szTemp,
		sizeof(szTemp),
		"\teth0 : %s\n\tsubnet : %s\n",
		szIpAddress,
		szSubnet
	);
	strncat(szBuffer, szTemp, sizeof(szBuffer));

	/* Close the bracket */
	snprintf(
		szTemp,
		sizeof(szTemp),
		"}\n\n\n"
	);
	strncat(szBuffer, szTemp, sizeof(szBuffer));


	lpszAllocOut = (LPSTR)calloc(0x01, sizeof(CHAR) * strlen(szBuffer) + 2);
	strcpy(lpszAllocOut, szBuffer);
	return lpszAllocOut;	
}




LPNODE_CORE
NODE::GetNodeCore(
	VOID
){ return &(this->ncCore); }




BOOL
NODE::SetIter(
	GtkTreeIter gtiIter
){
	this->gtiIter = gtiIter;
	return true;
}