#include "../Prereqs.h"
#include "Node.h"


class ROUTER : public NODE
{
	friend class NODE;

public:

	ROUTER(
		NODE 		*noParent, 
		const char 	*cszName
	);
	

	~ROUTER();

	
	

	char *
	DumpToString(
		VOID
	);

}
