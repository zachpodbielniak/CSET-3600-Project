#ifndef GUI_H
#define GUI_H


#include "Prereqs.h"
#include "Utils.h"


namespace Gui 
{


	BOOL
	CreateMainWindow(
		VOID
	);



	GtkWindow *
	ShowPropertiesWindow(
		LPVOID 		lpReserved
	);




	VOID
	ShowHelpWindow(
		VOID
	);




	VOID
	ShowAboutWindow(
		VOID
	);




	GtkBuilder *
	GetMainWindowBuilder(
		VOID
	);


}



#endif
