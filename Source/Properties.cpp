/*
 Author:        Dave Marley
 Summary:       This file handles the IP address properties module.

 Update: 	Zach Podbielniak - Changed the formatting of the file.
 Update:	Dave Marley - Added comments and noted that more Flags may need to be added.
*/
#include "Properties.h"
#include "Utils.h"
#include <iostream>

int test = 0xFF; // in binary 0b11111111.
BYTE ata[4] = {192,168,0,1};

void prop()
{
	ULONG ulFlags = 0;

	if(ulFlags & IP_FLAG){
	    	printf("IP ENABLED \n");
	}
	if(ulFlags & SUBNET_FLAG){
	    	printf("SUBNET ENABLED \n");
			std::cout << "255.255.255.0 \n";
			//Static Sub For now
	}
	if(ulFlags & GATE_FLAG){
	    	printf("GATE ENABLED \n");
	}
	if(ulFlags & MAC_FLAG){
	    	printf("MAC ENABLED \n");
	}
	if(ulFlags & HOST_FLAG){
	    	printf("HOST ENABLED \n");
	}
	if(ulFlags & HOSTNAME_FLAG){
	    	printf("HOSTNAME ENABLED \n");
	}
	if(ulFlags & DDNS_FLAG){
	   	 printf("DDNS ENABLED : ");
	    	std::cout << "8.8.8.8 \n";
			//Static dns For now
	}
	if(ulFlags & DHCP_FLAG){
	    	printf("DHCP ENABLED : ");	
	    	for(int x=0;x<4;x++){
	    	    	if(x<3){
	    	       		std::cout << ata[x];
	    	        	std::cout << ".";
	    	    	}else{
	    	        	std::cout << (ata[x] + 1);
						//add tree walk to verify that this auto incriments to an unused IP address
	    	    	}
	    	}
	    	printf("\n");
	}

}



