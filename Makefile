# File:		Makefile
# Last Update:	02/04/2018



CPP = g++
STD = -std=c++11
WARNINGS = -Wall -Wextra -Wno-write-strings
OPTIMIZE = -O3
MARCH = -march=native
GTK_CFLAGS = `pkg-config --cflags gtk+-3.0`
GTK_LIBS = `pkg-config --libs gtk+-3.0`

CPP_FLAGS = $(GTK_CFLAGS)
CPP_FLAGS += $(GTK_LIBS)
CPP_FLAGS += $(STD)
CPP_FLAGS += $(WARNINGS)
CPP_FLAGS += $(OPTIMIZE)
CPP_FLAGS += $(MARCH)


CPP_FLAGS_D = $(GTK_CFLAGS)
CPP_FLAGS_D += $(GTK_LIBS)
CPP_FLAGS_D += $(STD)
CPP_FLAGS_D += $(WARNINGS)
CPP_FLAGS_D += -D __DEBUG__

MAIN_FILES = Logger.o Gui.o Node.o Utils.o Properties.o Tree.o Import.o Export.o
MAIN_APP_FILES = $(MAIN_FILES)
MAIN_APP_FILES += Main.o
MAIN_TEST_FILES = $(MAIN_FILES)
MAIN_TEST_FILES += Test.o

MAIN_TEST_FILES_WIN += TestW.o Logger_t.o Gui_t.o Node_t.o Utils_t.o Properties_t.o
MAIN_APP_FILES_D = Main_d.o Logger_d.o Gui_d.o Node_d.o Utils_d.o Properties_d.o Tree_d.o Import_d.o Export_d.o





All: MainApp
Debug: MainApp_d


MainApp: $(MAIN_APP_FILES)
	$(CPP) -o Bin/MainApp Bin/*.o $(CPP_FLAGS)
	rm Bin/*.o


MainApp_d: $(MAIN_APP_FILES_D)
	$(CPP) -g -o Bin/MainApp_d Bin/*_d.o $(CPP_FLAGS_D)
	rm Bin/*.o


Test: $(MAIN_TEST_FILES)
	$(CPP) -o Bin/Test Bin/*.o $(CPP_FLAGS) -lboost_unit_test_framework


TestWin: $(MAIN_TEST_FILES_WIN)
	$(CPP) -o Bin/TestW Bin/*.o $(CPP_FLAGS)  /mingw64/lib/libboost_unit_test_framework-mt.a -D __TEST_SUITE_BUILD



Main.o:		Bin
	$(CPP) -fPIC -c -o Bin/Main.o Source/Main.cpp $(CPP_FLAGS)

Test.o: 	Bin
	$(CPP) -fPIC -c -o Bin/Test.o Source/Test.cpp $(CPP_FLAGS) -lboost_unit_test_framework

Logger.o:	Bin
	$(CPP) -fPIC -c -o Bin/Logger.o Source/Logger.cpp $(CPP_FLAGS)

	
Gui.o:		Bin
	$(CPP) -fPIC -c -o Bin/Gui.o Source/Gui.cpp $(CPP_FLAGS)


Node.o:		Bin
	$(CPP) -fPIC -c -o Bin/Node.o Source/Node/Node.cpp $(CPP_FLAGS)

Utils.o:	Bin
	$(CPP) -fPIC -c -o Bin/Utils.o Source/Utils.cpp $(CPP_FLAGS)

Properties.o:	Bin
	$(CPP) -fPIC -c -o Bin/Properties.o Source/Properties.cpp $(CPP_FLAGS)

Tree.o:		Bin
	$(CPP) -fPIC -c -o Bin/Tree.o Source/Tree.cpp $(CPP_FLAGS)

Import.o: 	Bin
	$(CPP) -fPIC -c -o Bin/Import.o Source/Import.cpp $(CPP_FLAGS)

Export.o: 	Bin
	$(CPP) -fPIC -c -o Bin/Export.o Source/Export.cpp $(CPP_FLAGS)




##########
## TEST ##
##########

TestW.o: 	Bin
	$(CPP) -fPIC -c -o Bin/Test.o Source/Test.cpp $(CPP_FLAGS) -D __TEST_SUITE_BUILD 


Logger_t.o:	Bin
	$(CPP) -fPIC -c -o Bin/Logger.o Source/Logger.cpp $(CPP_FLAGS) -D __TEST_SUITE_BUILD 

	
Gui_t.o:		Bin
	$(CPP) -fPIC -c -o Bin/Gui.o Source/Gui.cpp $(CPP_FLAGS) -D __TEST_SUITE_BUILD 


Node_t.o:		Bin
	$(CPP) -fPIC -c -o Bin/Node.o Source/Node/Node.cpp $(CPP_FLAGS) -D __TEST_SUITE_BUILD 

Utils_t.o:	Bin
	$(CPP) -fPIC -c -o Bin/Utils.o Source/Utils.cpp $(CPP_FLAGS) -D __TEST_SUITE_BUILD 

Properties_t.o:	Bin
	$(CPP) -fPIC -c -o Bin/Properties.o Source/Properties.cpp $(CPP_FLAGS) -D __TEST_SUITE_BUILD 




###################
# DEBUGGING FILES #
###################



Main_d.o:	Bin
	$(CPP) -g -fPIC -c -o Bin/Main_d.o Source/Main.cpp $(CPP_FLAGS_D)


Logger_d.o:	Bin
	$(CPP) -g -fPIC -c -o Bin/Logger_d.o Source/Logger.cpp $(CPP_FLAGS_D)


Gui_d.o:	Bin
	$(CPP) -g -fPIC -c -o Bin/Gui_d.o Source/Gui.cpp $(CPP_FLAGS_D)


Node_d.o:	Bin
	$(CPP) -g -fPIC -c -o Bin/Node_d.o Source/Node/Node.cpp $(CPP_FLAGS_D)

Utils_d.o:	Bin
	$(CPP) -g -fPIC -c -o Bin/Utils_d.o Source/Utils.cpp $(CPP_FLAGS_D)

Properties_d.o:	Bin
	$(CPP) -g -fPIC -c -o Bin/Properties_d.o Source/Properties.cpp $(CPP_FLAGS_D)

Tree_d.o:	Bin
	$(CPP) -g -fPIC -c -o Bin/Tree_d.o Source/Tree.cpp $(CPP_FLAGS_D)

Import_d.o: 	Bin
	$(CPP) -g -fPIC -c -o Bin/Import_d.o Source/Import.cpp $(CPP_FLAGS_D)

Export_d.o: 	Bin
	$(CPP) -g -fPIC -c -o Bin/Export_d.o Source/Export.cpp $(CPP_FLAGS_D)






Bin:
	mkdir -p Bin


clean:
	rm Bin/*
