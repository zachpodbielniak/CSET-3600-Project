# CSET-3600-Project
Main repo for our group's project for Spring 2018.

# How To Build (Linux)

+ Be sure to have the GTK+ 3 libs
+ Run "make -j" from root folder
	+ Alternatively run "make -j Debug" if you want to build the debug version
+ The built executable will be in the "Bin/" folder


# How To Build (Win32)

+ Follow the guide ("Documents/WinIntall.pdf") in setting up MSYS2
+ Run "make -j" from root folder
	+ Alternatively run "make -j Debug" if you want to build the debug version
+ The built executable will be in the "Bin/" folder
